<?php
namespace App\Http\Controllers;

class PagesController extends Controller
{
	public function contact()
	{
		return view('contact');
	}
	public function faq()
	{
		return view('faq');
	}
}